import os 
import json
from pathlib import Path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from sklearn.ensemble import IsolationForest
import torch
import torch.nn as nn
import torch.nn.functional as F
from tqdm.notebook import tqdm

import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor

def train_nn(
    model_nn, 
    timeseries_train_x_asfeatures, timeseries_train_y, 
    timeseries_test_x_asfeatures, timeseries_test_y, 
    trained_epochs, epochs_to_train,
    loss_function, optimizer,
    save_parameters=False):
    history = []
    
    device = 'cpu'
    size = len(timeseries_train_y)
    # x, y = timeseries_train_x[i].to(device), timeseries_train_y[i].to(device)

    epochs = epochs_to_train
    for epoch in (pbar := tqdm(range(epochs))):
        try:
            optimizer.zero_grad()
            pred = model_nn(timeseries_train_x_asfeatures)
            loss = loss_function(pred, timeseries_train_y)
            model_nn.train()
            loss.backward()
            optimizer.step()
    
            pred_test = model_nn(timeseries_test_x_asfeatures)
            loss_test = loss_function(pred_test, timeseries_test_y)
         
            if epoch == 0:
                continue
    
            k10 = epochs // 10 if epochs > 10 else 1  # every 10%
            k100 = epochs // 100 if epochs > 100 else (epochs // 10 if epochs > 10 else 1)  # every 1%
    
            if epoch == 0 or epoch % k100 == 0 or epoch == epochs:
                history.append({
                    'epoch': trained_epochs + epoch,
                    'loss': loss.item(),
                    'loss_test': loss_test.item(),
                    'parameters_array': np.concatenate([x.data.detach().numpy().flatten() for x in model_nn.parameters()]),
                    'state_dict': dict(model_nn.state_dict()),
                    'parameters': dict(
                        zip(model_nn.state_dict().keys(), [x.detach().numpy().flatten() for x in model_nn.parameters()])
                    )
                })
                pbar.set_description(f'[loss: {loss}]')
                
                
            if epoch == 0 or epoch % k10 == 0 or epoch == epochs:
                loss, current = loss.item(), epoch
                print(f"loss: {loss:>7f}  [{current:>5d}]")
                plt.figure()
                known = timeseries_train_y.flatten().detach().numpy()
                predicted = model_nn(timeseries_train_x_asfeatures).flatten().detach().numpy()
                plot_comparison(known, predicted)
        except KeyboardInterrupt as e:
            print('Keyboard interruption')
            break

    return history

# def plot_comparison(a, b):
#     plt.figure(figsize=[20, 3])
#     plt.plot(np.abs(a - b), '.')
#     plt.show()
    
def plot_comparison(a, b):
    plt.figure(figsize=[20, 3])
    plt.plot(a, b, '.')
    lim_min = min(a.min(), b.min())
    lim_max = max(a.max(), b.max())
    plt.plot([lim_min, lim_max], [lim_min, lim_max])
    plt.xlim(lim_min, lim_max)
    plt.ylim(lim_min, lim_max)
    plt.show()



def default_run(
    model, timeseries_train_x_asfeatures, timeseries_train_y, timeseries_test_x_asfeatures, timeseries_test_y, trained_epochs, epochs_to_train, save_parameters=False,
    loss_function=nn.L1Loss,
    optimizer=lambda params: torch.optim.Adam(params, lr=1e-3)
):
    history = train_nn(
        model, timeseries_train_x_asfeatures, timeseries_train_y, timeseries_test_x_asfeatures, timeseries_test_y, trained_epochs, epochs_to_train, 
        loss_function=loss_function(), optimizer=optimizer(model.parameters()),
        save_parameters=save_parameters
    )
    plot_comparison(
        timeseries_test_y.flatten().detach().numpy(),
        model(timeseries_test_x_asfeatures).flatten().detach().numpy()
    )
    plt.plot([h['epoch'] for h in history], [h['loss'] for h in history])
    return history


SAVES_BASE_DIR = 'model_states'

# def reset_model(model_name):#     files = sorted([f for f in os.listdir(SAVES_BASE_DIR + '/' + model_name)])
#     for file in files:
#         os.remove(file)



def save_torch(model, epochs, history):
    Path(SAVES_BASE_DIR + '/' + type(model).__name__).mkdir(parents=True, exist_ok=True)
    PATH = f'''{SAVES_BASE_DIR}/{type(model).__name__}/{type(model).__name__}.{epochs}.pt'''
    torch.save(model.state_dict(), PATH)
    save_history(SAVES_BASE_DIR, model, epochs, history)


def load_torch(ModelClass, kwargs):
    model = ModelClass(**kwargs)
    class_name = type(model).__name__
    basedir = SAVES_BASE_DIR + '/' + type(model).__name__ + '/'
    if os.path.exists(basedir):
        files = sorted([{'filename': fn, 'epochs': int(fn.split('.')[1])} for fn in os.listdir(path=basedir) if class_name in fn and 'pt' in fn], key=lambda x: int(x['epochs']))
    else:
        files = []
    if len(files) > 0:
        most_recent = files[-1]
        epochs = most_recent['epochs']
        model.load_state_dict(torch.load(basedir + most_recent['filename']))
        model.eval()
    else:
        epochs = 0
    return model, epochs




def get_total_history(basedir, class_name):
    files_history = sorted([f for f in os.listdir(basedir + '/' + class_name) if class_name in f and 'history' in f and '.npy' in f], key=lambda x: int(x.split('.')[1]))
    total_history = []
    for fname in files_history:
        # with open(basedir + '/' + fname, 'r') as f:
        total_history.extend(np.load(basedir + '/' + class_name + '/' + fname, allow_pickle=True))
    return total_history

def save_history(basedir, model, last_epoch, history):
    class_name = type(model).__name__
    filename = basedir + '/' + class_name + '/history_' + class_name + '.' + str(last_epoch) + '.npy'
    # with open(filename, 'w') as f:
    return np.save(filename, history)






